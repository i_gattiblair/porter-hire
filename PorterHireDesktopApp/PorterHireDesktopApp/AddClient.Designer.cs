namespace PorterHireDesktopApp
{
    partial class AddClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.CFname = new System.Windows.Forms.TextBox();
            this.CLname = new System.Windows.Forms.TextBox();
            this.CEmail = new System.Windows.Forms.TextBox();
            this.CCompanyName = new System.Windows.Forms.TextBox();
            this.CPhone = new System.Windows.Forms.TextBox();
            this.selectClientBtn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.ClientCombo = new System.Windows.Forms.ComboBox();
            this.deleteClientBtn = new System.Windows.Forms.Button();
            this.addClientBtn = new System.Windows.Forms.Button();
            this.updateClientBtn = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 26);
            this.label1.TabIndex = 4;
            this.label1.Text = "Add Client";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "First Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(34, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Last Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(34, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Email:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(34, 255);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Company Name:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(34, 287);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "Phone:";
            // 
            // CFname
            // 
            this.CFname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CFname.Location = new System.Drawing.Point(196, 153);
            this.CFname.Name = "CFname";
            this.CFname.Size = new System.Drawing.Size(370, 26);
            this.CFname.TabIndex = 10;
            // 
            // CLname
            // 
            this.CLname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CLname.Location = new System.Drawing.Point(196, 185);
            this.CLname.Name = "CLname";
            this.CLname.Size = new System.Drawing.Size(370, 26);
            this.CLname.TabIndex = 11;
            // 
            // CEmail
            // 
            this.CEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CEmail.Location = new System.Drawing.Point(196, 217);
            this.CEmail.Name = "CEmail";
            this.CEmail.Size = new System.Drawing.Size(370, 26);
            this.CEmail.TabIndex = 12;
            // 
            // CCompanyName
            // 
            this.CCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CCompanyName.Location = new System.Drawing.Point(196, 249);
            this.CCompanyName.Name = "CCompanyName";
            this.CCompanyName.Size = new System.Drawing.Size(370, 26);
            this.CCompanyName.TabIndex = 13;
            // 
            // CPhone
            // 
            this.CPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CPhone.Location = new System.Drawing.Point(196, 281);
            this.CPhone.Name = "CPhone";
            this.CPhone.Size = new System.Drawing.Size(370, 26);
            this.CPhone.TabIndex = 14;
            // 
            // selectClientBtn
            // 
            this.selectClientBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectClientBtn.Location = new System.Drawing.Point(196, 72);
            this.selectClientBtn.Name = "selectClientBtn";
            this.selectClientBtn.Size = new System.Drawing.Size(73, 31);
            this.selectClientBtn.TabIndex = 18;
            this.selectClientBtn.Text = "Select";
            this.selectClientBtn.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(13, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(157, 20);
            this.label7.TabIndex = 17;
            this.label7.Text = "Select Existing Client";
            // 
            // ClientCombo
            // 
            this.ClientCombo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClientCombo.FormattingEnabled = true;
            this.ClientCombo.Location = new System.Drawing.Point(12, 72);
            this.ClientCombo.Name = "ClientCombo";
            this.ClientCombo.Size = new System.Drawing.Size(179, 28);
            this.ClientCombo.TabIndex = 16;
            this.ClientCombo.SelectedIndexChanged += new System.EventHandler(this.ClientCombo_SelectedIndexChanged);
            // 
            // deleteClientBtn
            // 
            this.deleteClientBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteClientBtn.Location = new System.Drawing.Point(387, 342);
            this.deleteClientBtn.Name = "deleteClientBtn";
            this.deleteClientBtn.Size = new System.Drawing.Size(179, 28);
            this.deleteClientBtn.TabIndex = 21;
            this.deleteClientBtn.Text = "DELETE";
            this.deleteClientBtn.UseVisualStyleBackColor = true;
            this.deleteClientBtn.Click += new System.EventHandler(this.deleteClientBtn_Click);
            // 
            // addClientBtn
            // 
            this.addClientBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addClientBtn.Location = new System.Drawing.Point(477, 313);
            this.addClientBtn.Name = "addClientBtn";
            this.addClientBtn.Size = new System.Drawing.Size(89, 28);
            this.addClientBtn.TabIndex = 20;
            this.addClientBtn.Text = "ADD";
            this.addClientBtn.UseVisualStyleBackColor = true;
            this.addClientBtn.Click += new System.EventHandler(this.addClientBtn_Click);
            // 
            // updateClientBtn
            // 
            this.updateClientBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateClientBtn.Location = new System.Drawing.Point(387, 313);
            this.updateClientBtn.Name = "updateClientBtn";
            this.updateClientBtn.Size = new System.Drawing.Size(84, 28);
            this.updateClientBtn.TabIndex = 19;
            this.updateClientBtn.Text = "UPDATE";
            this.updateClientBtn.UseVisualStyleBackColor = true;
            this.updateClientBtn.Click += new System.EventHandler(this.updateClientBtn_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(793, 771);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(179, 28);
            this.button4.TabIndex = 22;
            this.button4.Text = "Back to menu";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // AddClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PorterHireDesktopApp.Properties.Resources.grey_light_smooth_moving_waves_background_video_animation_hd_1920x1080_vjkea4zve__F0000;
            this.ClientSize = new System.Drawing.Size(984, 811);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.deleteClientBtn);
            this.Controls.Add(this.addClientBtn);
            this.Controls.Add(this.updateClientBtn);
            this.Controls.Add(this.selectClientBtn);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ClientCombo);
            this.Controls.Add(this.CPhone);
            this.Controls.Add(this.CCompanyName);
            this.Controls.Add(this.CEmail);
            this.Controls.Add(this.CLname);
            this.Controls.Add(this.CFname);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddClient";
            this.Text = "AddClient";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox CFname;
        private System.Windows.Forms.TextBox CLname;
        private System.Windows.Forms.TextBox CEmail;
        private System.Windows.Forms.TextBox CCompanyName;
        private System.Windows.Forms.TextBox CPhone;
        private System.Windows.Forms.Button selectClientBtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox ClientCombo;
        private System.Windows.Forms.Button deleteClientBtn;
        private System.Windows.Forms.Button addClientBtn;
        private System.Windows.Forms.Button updateClientBtn;
        private System.Windows.Forms.Button button4;
    }
}